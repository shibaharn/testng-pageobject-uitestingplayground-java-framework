package TestCases;


import org.testng.annotations.Test;

public class UiTestingPlayGroundCases extends BaseTest {


    @Test(priority=1)
    public void TC_01_DYNAMIC_LINK_VALIDATION() throws InterruptedException {

        //{Scenario
        // Record button click.
        //Then execute your test to make sure that ID is not used for button identification.}
        homePage.dynamicIdClick();
        dynamicIdPage.verifydynamicIdButtonExists();
        dynamicIdPage.dynamicIdButtonClick();


    }

    // @Test(priority=2)
    // public void TC_02_DYNAMIC_WEB_TABLE_VALIDATION() {

    //     //{Scenario
    //     //For Chrome process get value of CPU load.
    //     //Compare it with value in the yellow label.}

    //     homePage.dynamicWebTableClick();
    //     dynamicWebTablePage.verifydynamicTableExists();
    //     dynamicWebTablePage.compareChromeCpuValues();

    // }


    // @Test(priority=3)
    // public void TC_03_ENTERING_TEXT_IN_OVERLAPPED_ELEMENT_VALIDATION() {

    //     //{Scenario
    //     //Record setting text into the Name input field (scroll element before entering the text).
    //     //Then execute your test to make sure that the text was entered correctly.}


    //     homePage.overLappedElementClick();
    //     overlappedElementPage.scrollPlayGroundPane();
    //     overlappedElementPage.enterTextInNameField("Shibahar");
    //     overlappedElementPage.validateTextInField();

    // }


    // // @Test(priority=4)
    // // public void TC04_TEXT_INPUT_VALIDATION() throws Exception {

    // //     //{Record setting text into the input field and pressing the button.
    // //     //Then execute your test to make sure that the button name is changing.}

    // //     homePage.textInputLinkClick();
    // //     textInputPage.validateButtonText("nametest");

    // // }


    // @Test(priority=5)
    // public void TC05_Non_Breaking_Space() {

    //     homePage.nonBreakingSpaceClick();
    //     nonBreakingSpacePage.validateButtonTextWithUniqueXpath();

    // }


    // @Test(priority=6)
    // public void TC06_VISIBILITY_TEST() {

    //     //{Scenario
    //     //Learn locators of all buttons.
    //     //In your testing scenario press Hide button.
    //     //Determine if other buttons visible or not.
    //     //}
    //     homePage.visiblityLinkClick();
    //     visibiltyPage.verifyButtonsDisplay();
    //     visibiltyPage.verifyButtonsInvisiblity();

    // }


    // @Test(priority=7)
    // public void TC07_MOUSE_HOVER_TEST() throws Exception {

    //     //{Record 2 consecutive link clicks.
    //     //Execute the test and make sure that click count is increasing by 2.}

    //     homePage.mouseHoverClick();
    //     mouseHoverPage.clickmeMouseHoverAndClick();
    //     mouseHoverPage.mousehoverPlayGround();
    //     mouseHoverPage.clickmeMouseHoverAndClick();
    //     mouseHoverPage.validateCount();

    // }


    // @Test(priority=8)
    // public void TC08_SAMPLE_APP_TEST() {

    //     //{Fill in and submit the form. For successfull login use any non-empty user name and `pwd` as password.}

    //     homePage.sampleAppClick();
    //     sampleAppPage.logInValidation("testuser", "pwd");
    //     sampleAppPage.logOutValidation();
    //     sampleAppPage.logInErrorValidation("testuser", "pwd123");

    // }


    // @Test(priority=9)
    // public void TC09_AJAX_DATA_TEST() throws Exception {

    //     //{Record the following steps. Press the button below and wait for data to appear (15 seconds), click on text of the loaded label.
    //     //Then execute your test to make sure it waits for label text to appear.}

    //     homePage.ajaxDataClick();
    //     ajaxDataPage.clickAjaxRequestButtonAndValidate();

    // }


    // @Test(priority=10)
    // public void TC10_LOAD_DELAY_TEST() throws Exception {

    //     //{Scenario
    //     //Navigate to Home page and record Load Delays link click and button click on this page.
    //     //Then play the test. It should wait until page is loaded.}

    //     homePage.loadDelayClick();
    //     loadDelayPage.waitForButtonLoad();

    // }


}
