package TestCases;

import Pages.*;
import driverManager.DriverManagerType;
import driverManager.WebDrivers;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import util.utility;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.Properties;

public class BaseTest {


    protected HomePage homePage;


    protected DynamicIdPage dynamicIdPage;

    protected DynamicWebTablePage dynamicWebTablePage;

    protected OverlappedElementPage overlappedElementPage;

    protected TextInputPage textInputPage;

    protected NonBreakingSpacePage nonBreakingSpacePage;

    protected MouseHoverPage mouseHoverPage;



    protected VisibiltyPage visibiltyPage;

    protected SampleAppPage sampleAppPage;

    protected AjaxDataPage ajaxDataPage;

    protected LoadDelayPage loadDelayPage;
    protected static WebDriver driver;
    Properties properties;
    static String URL;

    @BeforeSuite
    public void setUp() {

        String propertyPath = System.getProperty("user.dir") + "//src//test//resources//Env.properties";
        try {

            ChromeOptions options= new ChromeOptions();
            options.setHeadless(true);
            options.addArguments("window-size=1920,1200");
            //WebDriverManager.chromedriver().create();
            driver=WebDriverManager.chromedriver().capabilities(options).create();
            //driver = WebDrivers.getDriver(DriverManagerType.CHROME);
            properties = utility.loadProperties(propertyPath);
            URL = properties.getProperty("URL");

            homePage = new HomePage(driver);
            //childPage = new ChildPage(driver);
            dynamicIdPage= new DynamicIdPage(driver);
            dynamicWebTablePage= new DynamicWebTablePage(driver);
            overlappedElementPage= new OverlappedElementPage(driver);
            textInputPage= new TextInputPage(driver);
            nonBreakingSpacePage= new NonBreakingSpacePage(driver);
            visibiltyPage= new VisibiltyPage(driver);
            mouseHoverPage= new MouseHoverPage(driver);
            sampleAppPage= new SampleAppPage(driver);
            ajaxDataPage= new AjaxDataPage(driver);
            loadDelayPage= new LoadDelayPage(driver);

            //mcode
            driver.get(URL.toString());
            driver.manage().window().maximize();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @BeforeTest
    public void beforeAllTests()
    {
        System.out.println("Starting the test case execution");
    }


    @AfterMethod
    public void afterEveryTestCase(ITestResult result) throws InterruptedException
    {
       driver.navigate().back();
       Thread.sleep(2000);
        if (result.getStatus() == ITestResult.FAILURE) {
            driver.get(URL.toString());
        }


    }


    @BeforeTest
    public void AfterAllTests()
    {
        System.out.println("Testcases execution completed");
    }



    @AfterSuite
    public void destroyDriver() {
        driver.quit();
    }
}
